﻿var sockets = {};

sockets.init = function (app, server) {
    
    app.server = server;

    // socket.io setup
    io = require('socket.io').listen(server);
    
    var Stream = require('./eventhandlers/stream');
    
    sockets.allSockets = [];
    io.sockets.on('connection', function (socket) {
        
        console.log('socket connected from ');
        // Create event handlers for this socket
        var eventHandlers = {
            stream: new Stream(app, socket)
        };
        
        // Bind events to handlers
        for (var category in eventHandlers) {
            var handler = eventHandlers[category].handler;
            for (var event in handler) {
                socket.on(event, handler[event]);
            }
        }
        
        // Keep track of the socket
        sockets.allSockets.push(socket);
    });
    
};

module.exports = sockets;