﻿var net = require('net');

var Stream = function (app, socket) {
    this.app = app;
    this.socket = socket;
    this.client = null;
    
    // Expose handler methods for events
    this.handler = {
        'ping': ping.bind(this),
        'stream.setup': setup.bind(this)
    };
};

// Events

function setup(param) {
    var self = this;

    console.log('stream setup ' + param);
    
    var client = new net.Socket();
    this.client = client;
    var HOST = 'ftp.aceso.com', PORT = 21;
    client.connect(PORT, HOST, function () {
        console.log('CONNECTED TO: ' + HOST + ':' + PORT);

        setTimeout(function () {
            self.client.write('USER unifydemo\n');
        }, 3000);
        
        setTimeout(function () {
            self.client.write('PASS Unify@demo1\n');
        }, 5000);

        setInterval(function () {
            self.client.write('LIST\n');
        }, 30000);
    });
    
    client.on('data', function (data) {
        console.log('DATA: ' + data);
        self.socket.emit('message', data.toString());
    });
    
    client.on('close', function () {
        console.log('Connection closed');
    });

}

function ping() {
    // Reply to sender
    this.socket.emit('message', 'PONG!');
}

module.exports = Stream;